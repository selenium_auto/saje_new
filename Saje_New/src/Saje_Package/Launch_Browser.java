package Saje_Package;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.sun.jna.platform.unix.X11.Window;

public class Launch_Browser {

	WebDriver w = new FirefoxDriver();

	// w.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	void check() {
		w.manage().window().maximize();
		System.out.println("Browser launched and maximized");
	}

	void passURL() {
		String url = "http://dev.saje.com/us/combination-skin-care//";
		w.get(url);
		System.out.println("Storefront Loaded with product");
	}

	void openPLP() {
		System.out.println("Plp page loaded");
	}

	void openPDP() {
		w.findElement(By.xpath("//a[@class='name-link']")).click();

		System.out.println("pdp page loaded");
	}

	void addProductToCart() {
		w.findElement(By.id("add-to-cart")).click();
		System.out.println("product added to cart");
	}

	void closedPopup() {
		//w.findElement(By.xpath("//div[@class='modal-close']")).click();
		click();
		System.out.println("pop up closed");
	}



	private void click() {
		// TODO Auto-generated method stub
		
	}

	void cartPage() {
		w.findElement(By.xpath("//a[@class='button mini-cart-link-cart']")).click();
		System.out.println("Cart page loaded");
	}

	void closingBrowser() {
		w.close();
		System.out.println(" Application sucesfully loaded and browser closed ");
	}

}
